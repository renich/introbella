#! /usr/bin/env bash
# Last Update: Sat 07 May 2011 07:02:29 AM CDT
# introbella - wav2OggMp3Flac converter
# GPLv3 or >
#
# This script depends on ffmpeg, lame, oggenc and flac in order to work.
#
# INSTRUCTIONS
# 1. Make a directory and subdirectories with the following layout
# BandName-Album (or BandName/Album... it's up to you)
# |-master
# |--48kHz_at_24bit
#
# 1.1 Put the songs in WAV format; in 48 kHz @ 24 bit quality in
#     the masters/48kHz_at_24bit directory
#
# 1.2. I recommend you use a natural songname format for the songs: song 1.wav,
#      song 2.wav, song 3.wav
#
# 2. Run the script like this:
#    wav2OggMp3Flac /path/to/BandNmae-Album/master/48kHz_at_24bit
#
# 3. Go get a coca-cola or something.
#
# TODO: Get Album info (band, album, year and calculate name based on fileane.
#       Clean up if _ or - used.)
# TODO: Add an if file exists verification so it doesn't overwrite the files
#       every time. Add a size verification (if not zero or something)
# TODO: Reimplement using SoX (DONE)

main_path="$1"

if [[ -d "$main_path" ]]; then
    for song in "$main_path"/*.wav;
    do
        # separate path from song name
        path="${song%/*}"
        song="${song##*/}"

        # convert to wav in 44.1kHz at 16bit
        pathWav44="$path/../44.1kHz_at_16bit"
        mkdir -p "$pathWav44"
        sox --norm --multi-threaded --show-progress "${path}/${song}" -b 16 -r 44100 "${pathWav44}/${song%.*}.wav" dither silence 1 0.5 0.1%

        # convert wav to ogg (highest quality)
        pathOgg48="$main_path/../../ogg/48kHz"
        pathOgg44="$main_path/../../ogg/44.1kHz"
        mkdir -p "$pathOgg44" "$pathOgg48"
        sox --norm --multi-threaded --show-progress "${path}/${song}" -C 10 "${pathOgg48}/${song%.*}.ogg" dither silence 1 0.5 0.1%
        sox --norm --multi-threaded --show-progress "${path}/${song}" -C 5 -r 44100 "${pathOgg44}/${song%.*}.ogg" dither silence 1 0.5 0.1%

        # convert wav to mp3 (highest quality)
        pathMp348="$path/../../mp3/48kHz"
        pathMp344="$path/../../mp3/44.1kHz"
        mkdir -p "$pathMp344" "$pathMp348"
        lame --resample 48 --bitwidth 24 --replaygain-accurate -q 0 -V 0 --preset insane "$path/$song" "$pathMp348/${song%.*}.mp3";
        lame --resample 44.1 --bitwidth 16 --replaygain-accurate -q 0 -b 128 "$pathWav44/$song" "$pathMp344/${song%.*}.mp3";

        # convert wav to flac (highest quality)
        pathFlac44="$main_path/../../flac/44.1kHz_at_16bit"
        pathFlac48="$main_path/../../flac/48kHz_at_24bit"
        mkdir -p "$pathFlac44" "$pathFlac48"
        sox --norm --multi-threaded --show-progress "${path}/${song}" -C 8 -b 16 -r 44100 "${pathFlac44}/${song%.*}.flac" dither silence 1 0.5 0.1%
        sox --norm --multi-threaded --show-progress "${path}/${song}" -C 8 -b 24 -r 48000 "${pathFlac48}/${song%.*}.flac" dither silence 1 0.5 0.1%
    done
else
    echo "The given path is not a directory. Please, provide a directory containing the master (wav) files"
    exit 1
fi
exit 0
