#
# introbella.com
# Sun 08 Apr 2007 04:20:12 PM CDT
# License: GPL 3 or >

All these scripts are property of introbella; and are distributed under the terms of the GPL license; which you can verify here:

# Official website
http://www.gnu.org/licenses/gpl.html

# Versión en español
http://www.garaitia.com/new/gpl-spanish.php

Renich Bon Ciric <renich@introbella.com>
